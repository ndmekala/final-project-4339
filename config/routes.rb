Rails.application.routes.draw do
  resources :stages

  resources :statuses

  resources :appointments


  resources :patients

  resources :employees

  resources :physicians

  resources :diagnostics

  resources :insurances

  mount Upmin::Engine => '/admin'
  root to: 'visitors#index'
  devise_for :users
  resources :users
end
