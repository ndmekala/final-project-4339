json.array!(@diagnostics) do |diagnostic|
  json.extract! diagnostic, :id, :code, :name, :fee
  json.url diagnostic_url(diagnostic, format: :json)
end
