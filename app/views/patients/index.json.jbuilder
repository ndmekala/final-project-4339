json.array!(@patients) do |patient|
  json.extract! patient, :id, :firstname, :lastname, :address, :city, :state, :zip, :phone, :email, :insurance_id, :insurancepolicynumber
  json.url patient_url(patient, format: :json)
end
