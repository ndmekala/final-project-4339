json.array!(@employees) do |employee|
  json.extract! employee, :id, :firstname, :lastname, :address, :city, :state, :zip, :email
  json.url employee_url(employee, format: :json)
end
