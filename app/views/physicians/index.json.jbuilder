json.array!(@physicians) do |physician|
  json.extract! physician, :id, :firstname, :lastname, :address, :city, :state, :zip, :email
  json.url physician_url(physician, format: :json)
end
