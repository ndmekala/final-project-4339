json.extract! @appointment, :id, :patient_id, :physician_id, :reason, :date, :time, :notes, :diagnostic_id, :created_at, :updated_at
