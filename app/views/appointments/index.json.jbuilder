json.array!(@appointments) do |appointment|
  json.extract! appointment, :id, :patient_id, :physician_id, :reason, :date, :time, :notes, :diagnostic_id
  json.url appointment_url(appointment, format: :json)
end
