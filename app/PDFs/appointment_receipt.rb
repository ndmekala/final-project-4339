class AppointmentReceipt < Prawn::Document

def initialize(appointment, view)
  super(top_margin: 50)
  @appointment = appointment

    order_number
    end
end

  def order_number
    text "Invoice Number  \##{@appointment.id}", size: 30

    patient_information
    appointment_information

    text "Total Fee Amount: $#{@appointment.diagnostic.fee}0", size: 15

  end


  def patient_information
    move_down (10)

    bounding_box([25,bounds.top-50], :width => 516, :height => 680, :align => :left) do
      text "Patient", :style => :bold

    patientData =
    [["#{@appointment.patient.firstname}"],
    ["#{@appointment.patient.lastname}"],
    ["#{@appointment.patient.phone}"],
    ["#{@appointment.patient.email}"]]

    table(patientData,:row_colors => ["FDFFFF", "FFFFFF"], :cell_style => { :border_width => 0,:border_color=> 'C1C1C1' }) do |table|
    end
    end

    bounding_box([350,bounds.top-50], :width => 516, :height => 680, :align => :right) do
      text "Physician", :style => :bold
    physicianData =
        [["#{@appointment.physician.firstname}"],
         ["#{@appointment.physician.lastname}"],
         ["1800-555-5555"],
         ["contact@dchouston.com"]]

    table(physicianData,:row_colors => ["FDFFFF", "FFFFFF"], :cell_style => { :border_width => 0,:border_color=> 'C1C1C1' }) do |table|
    end
    end


  end


  def appointment_information

    bounding_box([25,bounds.top-250], :width => 516, :height => 680, :align => :center) do
      text "Appointment", :style => :bold, size: 20

      appointmentData=
          [["Time","#{@appointment.appointmenttime.strftime('%a %m/%d/%y   %H:%M %P')}", ""],
           ["Reason", "#{@appointment.reason}", ""],
           ["Diagnostic", "#{@appointment.diagnostic.name}", ""],
           ["","Fee", "$#{@appointment.diagnostic.fee}0"]]
      table(appointmentData,:row_colors => ["FDFFFF", "FFFFFF"], :cell_style => { :border_width => 0.2,:border_color=> 'C1C1C1' }) do |table|
      end

    end
end

