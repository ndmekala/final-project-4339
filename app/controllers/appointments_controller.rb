class AppointmentsController < ApplicationController
  before_action :set_appointment, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @appointments = Appointment.all
    respond_with(@appointments)
  end

  def show
    @appointment = Appointment.find(params[:id])
    respond_to do |format|
      format.html
      format.pdf do
        pdf = AppointmentReceipt.new(@appointment, view_context)
        send_data pdf.render, filename: "Appointment_Invoice_#{@appointment.id}.pdf",
                    type: "application/pdf",
                    disposition: "inline"
      end
    end

  end

  def new
    @appointment = Appointment.new
    respond_with(@appointment)
  end

  def edit
  end

  def create
    @appointment = Appointment.new(appointment_params)
    flash[:notice] = 'Appointment was successfully created.' if @appointment.save
    respond_with(@appointment)
  end

  def update
    flash[:notice] = 'Appointment was successfully updated.' if @appointment.update(appointment_params)
    respond_with(@appointment)
  end

  def destroy
    @appointment.destroy
    respond_with(@appointment)
  end

  private
    def set_appointment
      @appointment = Appointment.find(params[:id])
    end

    def appointment_params
      params.require(:appointment).permit(:patient_id, :reason, :appointmenttime, :physician_id, :notes, :diagnostic_id, :stage_id)
    end
end
