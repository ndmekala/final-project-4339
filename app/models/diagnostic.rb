class Diagnostic < ActiveRecord::Base

  has_many :appointments

  validates_presence_of :fee
  validate(:fee, :numericality => {:greater_than_equal_to => 0, :message => 'Fee cannot be negative'})
end
