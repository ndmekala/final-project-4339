class Patient < ActiveRecord::Base

  has_many :appointments
  belongs_to :insurance
  has_many :physicians, through: :appointments
  has_one :user

  def fullname
    firstname + ' ' + lastname
  end

end
