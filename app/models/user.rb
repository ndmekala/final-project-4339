class User < ActiveRecord::Base
  enum role: [:admin, :physician, :employee, :patient]
  after_initialize :set_default_role, :if => :new_record?

  def set_default_role
    self.role ||= :patient
  end

  belongs_to :patient

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
end
