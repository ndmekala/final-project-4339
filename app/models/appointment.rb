class Appointment < ActiveRecord::Base

  belongs_to :diagnostic
  belongs_to :patient
  belongs_to :physician
  belongs_to :stage

  validate :appointmenttime, uniqueness: true

end
