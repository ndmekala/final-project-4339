class AddStageToAppointment < ActiveRecord::Migration
  def change
    add_column :appointments, :stage_id, :integer
  end
end
