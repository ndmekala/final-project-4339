class RemoveStatusIdFromAppointment < ActiveRecord::Migration
  def change

    remove_column :appointments, :status_id, :integer

  end
end
