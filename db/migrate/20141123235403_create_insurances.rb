class CreateInsurances < ActiveRecord::Migration
  def change
    create_table :insurances do |t|
      t.string :insurancename
      t.string :insurancepolicynumber

      t.timestamps
    end
  end
end
