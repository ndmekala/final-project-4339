class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer :patient_id
      t.string :reason
      t.datetime :appointmenttime
      t.integer :physician_id
      t.string :notes
      t.integer :diagnostic_id

      t.timestamps
    end
  end
end
