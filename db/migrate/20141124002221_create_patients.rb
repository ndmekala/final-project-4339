class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string :firstname
      t.string :lastname
      t.string :address
      t.string :city
      t.string :state
      t.string :zip
      t.integer :phone
      t.string :email
      t.integer :insurance_id
      t.string :insurancepolicynumber

      t.timestamps
    end
  end
end
