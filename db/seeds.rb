# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#user = CreateAdminService.new.call
#puts 'CREATED ADMIN USER: ' << user.email


require 'csv'

Diagnostic.delete_all

open("/vagrant/projects/DCHouston/code.csv") do |diagnostics|
  diagnostics.read.each_line do |diagnostics|
    code, name, fee = diagnostics.chomp.split(",")
    Diagnostic.create!(:name => name, :code => code, :fee => fee)
  end
end
